# esqueleto

Type-safe EDSL for SQL queries on persistent backends. [esqueleto](http://hackage.haskell.org/package/esqueleto)

Debian: [haskell-esqueleto](https://tracker.debian.org/pkg/haskell-esqueleto)

# Books
* Developing Web Apps with Haskell and Yesod, 2nd Edition (Ch. 19. SQL Joins)

# Unofficial documentation
* [*Esqueleto and Complex Queries*
  ](https://mmhaskell.com/real-world/esqueleto)
  (2022) Monday Morning Haskell
